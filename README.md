#Dwm Dynamic Window Manager
--------------------------

This is my build of the latest 6.2 dwm.
Paches already patched.
* autostart
* fakefullscreen
* cycle layout
* useless gaps

#Build Instructions
-------------------
make
sudo make install.

#Pre-requrements
GCC, X11. (Picom for transparency & gaps), make.
